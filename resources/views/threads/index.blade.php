<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Forum | Laravel.io</title>
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body class="bg-grey-lighter font-sans leading-none text-smooth">
        <header class="bg-light border-b mb-4">
            <div class="container mx-auto">
                <div class="flex border-b">
                    <div class="w-1/4 flex items-center">
                        <img src="{{ asset('images/logo.png') }}" alt="logo" class="w-32">
                    </div>
                    <div class="w-1/2 nav">
                        <a href="#" class="nav-item nav-item--active">Forum</a>
                        <a href="#" class="nav-item">Pastebin</a>
                        <a href="#" class="nav-item">Chat</a>
                        <a href="#" class="nav-item">Jobs</a>
                        <a href="#" class="nav-item">Community</a>
                    </div>
                    <div class="w-1/4 text-right">
                        <a href="#" class="inline-block mr-8 py-6 text-grey-dark">Login</a>
                        <a href="#" class="inline-block py-6 text-grey-dark">Register</a>
                    </div>
                </div>
                <div class="flex justify-between items-center py-4">
                    <h1 class="text-regular text-xl">Forum</h1>
                    <div>
                        <div class="relative">
                            <input class="appearance-none border-2 pl-12 pr-4 px-4 rounded" type="text" placeholder="Search for threads...">
                            <div class="absolute flex items-center pin-l pin-y px-4">
                                @include('svg.search', ['class' => 'h-4 text-grey w-4'])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container mx-auto">
            <div class="flex -mx-4">
                <div class="px-4 w-3/4">
                    <div class="rounded bg-light">
                        @foreach ($threads as $thread)
                            <div class="border-b pl-4 pr-16 py-4 relative">
                                <div class="mb-4">
                                    <h2 class="mb-2 text-lg truncate">{{ $thread['title'] }}</h2>
                                    <p class="mb-2 text-grey-dark text-sm">{{ $thread['preview'] }}</p>
                                </div>
                                <div class="flex">
                                    <div class="flex items-center mr-6 text-grey-dark text-xs">
                                        @include('svg.user-solid-circle', ['class' => 'h-5 text-grey w-5 mr-2'])
                                        <p>
                                            <a href="#" class="text-green-dark">{{ $thread['author'] }}</a>
                                            replied 12 minutes ago
                                        </p>
                                    </div>
                                    <div>
                                        @foreach ($thread['tags'] as $tag)
                                            <span class="badge">{{ $tag }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="absolute flex items-center mr-5 mt-5 pin-r pin-t text-xs">
                                    @include('svg.chat-bubble-dots', ['class' => 'h-3 text-grey w-3 mr-1'])
                                    {{ $thread['reply_count'] }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="px-4 w-1/4">
                    <div class="py-2">
                        <div class="mb-8">
                            <a href="#" class="bg-green hover:bg-green-dark block py-3 px-4 rounded text-bold text-center text-light">Create Thread</a>
                        </div>
                        <div>
                            <h3 class="mb-2 text-grey-dark text-xs tracking-wide uppercase">tags</h3>
                            <ul class="list-reset">
                                    <a href="#" class="bg-grey-lightest block capitalize mb-1 px-3 py-2 rounded font-bold text-sm">
                                        <li>{{ $tags[0] }}</li>
                                    </a>
                                @foreach (collect($tags)->slice(1) as $tag)
                                    <a href="#" class="block capitalize mb-1 px-3 py-2 text-grey-dark text-sm">
                                        <li>{{ $tag }}</li>
                                    </a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>