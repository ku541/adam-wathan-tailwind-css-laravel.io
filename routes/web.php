<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/forum', function () {
    $threads = [
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 0,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 3,
            'tags'          => ['lorem']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 2,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 0,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 11,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 0,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 0,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 1,
            'tags'          => ['lorem']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 1,
            'tags'          => ['lorem']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 0,
            'tags'          => ['lorem']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 4,
            'tags'          => ['lorem', 'ipsum']
        ],
        [
            'title'         => 'Lorem ipsum dolor sit amet',
            'preview'       => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'author'        => 'Lorem',
            'reply_count'   => 3,
            'tags'          => ['lorem', 'ipsum']
        ]
    ];

    $tags = [
        'lorem',
        'ipsum',
        'dolor',
        'sit',
        'amet',
        'consectetur',
        'adipisicing',
        'elit',
        'eaque',
        'veritatis',
        'ab',
        'commodi',
        'hic',
        'dolor',
        'doloremque',
        'recusandae'
    ];

    return view('threads.index', ['threads' => $threads, 'tags' => $tags]);
});
